
from typing import Optional, List, Set, Tuple, Dict, Union, Any
from datasets import IterableDataset
from transformers import default_data_collator

from .perplexity import EvalSet, log_perplexity

from torch.utils.data import DataLoader
from torch.optim import Optimizer
from transformers import get_scheduler
from torch.optim.lr_scheduler import LRScheduler
from transformers import PreTrainedModel
from transformers.trainer_utils import SchedulerType
from accelerate import Accelerator
from tqdm.auto import tqdm

import torch

class LMAccelTraining:
    def __init__(self,
            eval_set : EvalSet,
            optimizer : Optimizer,
            ) -> None:
        self.eval_set = eval_set
        self.optimizer = optimizer
    def one_epoch(self,
                model : PreTrainedModel,
                train_dataloader : DataLoader,
                scheduler : LRScheduler,
                accelerator : Accelerator,
                optimizer : Optimizer,
                progress_bar : Optional[tqdm]=None
                ) -> None:
        model.train()
        for batch in train_dataloader:
            outputs = model(**batch)
            loss = outputs.loss
            accelerator.backward(loss)

            optimizer.step()
            scheduler.step()
            optimizer.zero_grad()
            if progress_bar:
                progress_bar.update(1)
        return
    def train(self,
                model : PreTrainedModel,
                train_dataloader : DataLoader,
                train_count : int,
                scheduler_name : Union[str, SchedulerType],
                num_epochs : int=1,
                addl_scheduler_kws : Dict[str, Any]=dict(),
            ) -> None:
        num_training_steps = num_epochs * train_count
        scheduler = get_scheduler(scheduler_name,
                optimizer=self.optimizer,
                num_training_steps=num_training_steps,
                **addl_scheduler_kws)
        accelerator = Accelerator()
        eval_dataloader = self.eval_set.data_loader()
        (model, eval_dataloader, train_dataloader, optimizer) = \
                accelerator.prepare(model,
                eval_dataloader,
                train_dataloader,
                self.optimizer)
        for epoch in range(num_training_steps):
            self.one_epoch(model, train_dataloader, 
                    scheduler, accelerator,
                    optimizer)
            lp = log_perplexity(model, eval_dataloader,
                    accelerator,
                    eval_units=self.eval_set.eval_units,
                    batch_size=self.eval_set.batch_size)
            print(f'after epoch {epoch}, log_perplexity = {lp}')






# vim: si ai


