from transformers import AutoModelForMaskedLM, AutoTokenizer


def base_model_and_tokenizer(base='distilbert-base-uncased'):
    model = AutoModelForMaskedLM.from_pretrained(base)
    tokenizer = AutoTokenizer.from_pretrained(base)
    return model, tokenizer


# vim: si ai
