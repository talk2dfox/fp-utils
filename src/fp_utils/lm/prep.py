"""
prepare text for fine-tuning an LM
"""
import numpy as np
from transformers import default_data_collator

def get_tokenizer_fn(tokenizer):
    def tokenizer_fn(text_ds):
        result = tokenizer(text_ds['text'])
        if tokenizer.is_fast:
            result['word_ids'] = [result.word_ids(i) 
                    for i in range(len(result['input_ids']))]
        return result
    return tokenizer_fn
                
def tokenized(tokenizer, text_ds):
    return text_ds.map(get_tokenizer_fn(tokenizer), 
            batched=True, remove_columns=['text'])


def model_max_tokens(tokenizer):
    return tokenizer.model_max_length


def truncate_token_features(features, orig_tokens, limit, debug=0):
    if debug:
        print(f'truncating {orig_tokens} to {limit}')
    for feature in list(features):
        if debug > 1:
            print(f'truncating {feature}')
        if len(features[feature]) == orig_tokens:
            features[feature] = features[feature][:limit]
#    return default_data_collator(features)
    return features

def find_word_boundary_for_truncation(word_ids, max_tokens):
    word_id_to_drop = word_ids[max_tokens]
    trunc_index = word_ids.index(word_id_to_drop)
    return trunc_index


def get_text_truncator(max_tokens, debug=0):
    if debug:
        print(f'truncator: {max_tokens}')
    def whole_word_truncation(features):
        n = len(features['input_ids'])
        if debug > 1:
            print(f'wwt: {n} input_ids')
        if n <= max_tokens:
#            return default_data_collator(features)
            if debug > 1:
                print(f'{n} < {max_tokens}, skipping truncation')
            return features
        if 'word_ids' in features:
            trunc_index = find_word_boundary_for_truncation(features['word_ids'], max_tokens)
        else:
            trunc_index = max_tokens
        return truncate_token_features(features, n, trunc_index)
    return whole_word_truncation


def truncated_tokens(tokenized, max_tokens, debug=0, **kw):
    truncator = get_text_truncator(max_tokens, debug=debug)
    return tokenized.map(truncator, 
            batched=False, **kw)


def get_text_padder(max_tokens, pad_token_id, debug=0):
    if debug:
        print(f'padder: {max_tokens}')
    def padding(features):
#        print(batch['input_ids'][:10])
        n = len(features['input_ids'])
        if n < max_tokens:
            if debug > 1:
                print(f'pad {n} to {max_tokens}')
            features['input_ids'].extend((pad_token_id,)*(max_tokens - n)) 
            for k in features:
                if k != 'input_ids':
                    features[k].extend((0,)*(max_tokens-n))
            if debug > 1:
                print(len(features['input_ids']))
        return features
    return padding

def padded_tokens(truncated, max_tokens, pad_token_id, debug=0):
    padder = get_text_padder(max_tokens, pad_token_id, debug=debug)
    return truncated.map(padder, batched=False)




def old_get_text_truncator(max_tokens):
    def truncate_text(text):
        """
        dead simplest way to ensure <= max tokens - just truncate everything past
        that (throwing away data)
        """
        text['orig_ids'] = text['input_ids']
        if 'word_ids' in text:
            text['orig_word_ids'] = text['word_ids']
        if len(text['input_ids']) <= max_tokens:
            return text
        raw = text['orig_ids'][:max_tokens]
        if 'word_ids' in text:
            # want to truncate at a word boundary
            # first word not fully included
            truncated_word = text['word_ids'][max_tokens]
            trunc_index = text['word_ids'].index(truncated_word)
            text['word_ids'] = text['word_ids'][:trunc_index]
            text['input_ids'] = text['input_ids'][:trunc_index]
        return text
    return truncate_text





# vim: si ai
