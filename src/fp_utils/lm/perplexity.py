"""
calculate perplexity for a given sample
"""

from typing import Optional, List, Set, Tuple, Dict, Union
from datasets import IterableDataset
from transformers import default_data_collator

from torch.utils.data import DataLoader
from transformers import PreTrainedModel
from accelerate import Accelerator

import torch

def log_perplexity(prepared_model : PreTrainedModel,
        prepared_loader : DataLoader,
        accelerator : Accelerator,
        eval_units : int,
        batch_size : int=64,
        verbose : int=0) -> float:
    losses = []
    prepared_model.eval()
    for step, batch in enumerate(prepared_loader):
        if verbose:
            print(f'step: {step}')
        with torch.no_grad():
            outputs = prepared_model(**batch)
        loss = outputs.loss
        losses.append(accelerator.gather(loss.repeat(batch_size)))
    losses = torch.cat(losses)
    losses = losses[:eval_units]
    return torch.mean(losses)


class EvalSet:
    def __init__(self, eval_set: IterableDataset,
            eval_units : int,
            batch_size : int=64) -> None:
        self.eval_units = eval_units
        self.eval_list = list(eval_set)
        self.batch_size = batch_size
    def data_loader(self) -> DataLoader:
        return DataLoader(self.eval_list, 
                batch_size=self.batch_size,
                collate_fn=default_data_collator)
    def log_perplexity(self,
            model : PreTrainedModel,
            accelerator : Optional[Accelerator] = None,
            verbose : int=0) -> float:
        dl = self.data_loader()
        accelerator = accelerator or Accelerator()
        model, eval_dataloader = accelerator.prepare(model, dl)
        return log_perplexity(prepared_model=model,
                prepared_loader=eval_dataloader,
                accelerator=accelerator,
                eval_units=self.eval_units,
                batch_size=self.batch_size,
                verbose=verbose)


# vim: si ai

