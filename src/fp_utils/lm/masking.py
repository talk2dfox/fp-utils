"""
masking for masked LM
"""

import collections

import numpy as np
from transformers import default_data_collator


def get_ww_masking_data_collator(tokenizer, rgen, wwm_probability= 0.2):
    def whole_word_masking_data_collator(features):
        realized = []
        for i, feature in enumerate(features):
            word_ids = feature.pop("word_ids")
            if 'word_ids' in feature:
                print('-------word_ids not POPPED-----')

            # Create a map between words and corresponding token indices
            mapping = collections.defaultdict(list)
            current_word_index = -1
            current_word = None
            for idx, word_id in enumerate(word_ids):
                if word_id is not None:
                    if word_id != current_word:
                        current_word = word_id
                        current_word_index += 1
                    mapping[current_word_index].append(idx)

            # Randomly mask words
            mask = rgen.binomial(1, wwm_probability, (len(mapping),))
            input_ids = feature["input_ids"]
            if 'labels' not in feature:
                feature["labels"] = word_ids
            labels = feature["labels"]
            new_labels = [-100] * len(labels)
            for word_id in np.where(mask)[0]:
                word_id = word_id.item()
                for idx in mapping[word_id]:
                    new_labels[idx] = labels[idx]
                    input_ids[idx] = tokenizer.mask_token_id
            feature["labels"] = new_labels

            for k in feature:
                if set(type(el) for el in feature[k]) != set([type(0)]):
                    print(f'in feature {k} of element {i}, found non integer')
            if 'word_ids' in feature:
                print('-------word_ids not POPPED-----')
            realized.append(feature)

        return default_data_collator(realized)
    return whole_word_masking_data_collator


def mask_function(data_collator):
    def insert_random_mask(batch):
        features = [dict(zip(batch, t)) for t in zip(*batch.values())]
#        print(features)
        masked_inputs = data_collator(features)
        # Create a new "masked" column for each column in the dataset
        return {"masked_" + k: v.numpy() for k, v in masked_inputs.items()}
    return insert_random_mask

def mask_set(tokenizer, unmasked_set, rgen, wwm_probability=0.2):
    wwdc = get_ww_masking_data_collator(tokenizer, rgen,
            wwm_probability=wwm_probability)
    masker = mask_function(wwdc)
    ds = unmasked_set.map(
            masker,
            batched=True
            , remove_columns=['input_ids', 'attention_mask', 'word_ids']
#            remove_columns=unmasked_set.column_names
            )
    ds = ds.rename_columns(
            {
                "masked_input_ids": "input_ids",
                "masked_attention_mask": "attention_mask",
                "masked_labels": "labels",
            }
            )
    return ds



# vim: si ai
