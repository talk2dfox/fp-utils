from pathlib import Path


def input():
    return Path('/kaggle/input')

def contest():
    return "feedback-prize-evaluating-student-writing"

def contest_path():
    return input() / contest()

