import os

from .data_dir import contest_path
from datasets import load_dataset

def essay_directory(train=True):
    return contest_path() / ("train" if train else "test")

def avail_essays(essay_dir):
    return os.listdir(essay_dir)


def text_dataset(train=True):
    essay_dir = essay_directory(train=train)
    essays = sorted(avail_essays(essay_dir))
    return load_dataset("text", 
            data_files = {'train': [os.path.join(essay_dir, fn) for fn in essays]},
            sample_by="document", 
            streaming=True)

def old_text_dataset(train=True):
    essay_dir = essay_directory(train=train)
    return load_dataset("text", 
            data_dir=essay_dir, 
            sample_by="document", 
            streaming=True)
