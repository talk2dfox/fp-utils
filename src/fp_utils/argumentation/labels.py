import pandas as pd

from ..input.data_dir import contest_path

def label_file():
    return contest_path() / 'train.csv'

def train_meta():
    with open(label_file()) as labs:
        meta = pd.read_csv(labs, 
                dtype = {
                    'discourse_id': int, 
                    'discourse_start': int, 
                    'discourse_end': int})
                }
                )
    return meta




